<?php
    session_start();
    
    $erreurs = array();
    
    while(list($id,$value)=each($_POST))
    {
        if(empty($value))
        {
            $erreurs[]="Le champ ".$id." est vide, bitch!";
        }
    }
    extract($_POST);
    
    $exprRegAnneeConstr='~^[0-9]{1,4}$~';
    if(!preg_match($exprRegAnneeConstr,$anneeConstr))
    {
        $erreurs[]="Seulement des chiffres pour l'année, bitch!";
    }
    
    if(count($erreurs)!=0)
    {
       echo "<p class='erreur'>";
        for($i = 0; $i<count($erreurs);$i++)
        {
            echo($i+1)." - ".$erreurs[$i]."<br/>";
        }
        echo "</p>";
    }
    else
    {
        
        if (!file_exists("inscrits.xml"))
        // construction de la racine quand le fichier n’existe pas
        $racine = new SimpleXMLElement ("<voiliers></voiliers>");
        else // lecture de la racine comme un element xml
            $racine = simplexml_load_file ("inscrits.xml");
        $voilier = $racine->addChild ("voilier");
        $voilier->addAttribute("numeroDeVoile", $numVoile);
        $voilier->addChild ("nom", $nomVoilier);
        $voilier->addAttribute("classe", $classe);
        $voilier->addChild("architecte",$chantier);
        $voilier->addAttribute("anneeFabrication",$anneeConstr);
        $photo = $voilier->addChild("photo");
        $photo->addAttribute("adresse", $adressePhoto);
        $voilier->addChild("chantierNaval",$chantier);
        $skippers = $voilier->addChild("skippers");
        $skipper = $skippers->addChild("skipper");
        $skipper->addAttribute("nom",$skipper1);
        $skipper = $skippers->addChild("skipper");
        $skipper->addAttribute("nom",$skipper2);
        $skippers->addChild("texteSkipper",$skippersDescr);
        $nfile = fopen("inscrits.xml", "w");
        fwrite ($nfile , $racine->asXML());
        fclose ($nfile);
    }
?>