window.onload = initPage; //Au chargement de la page faire la méthode initPage

function initPage() 
{
    document.getElementById("envoi").onclick = envoiDonnees;
}

function envoiDonnees()
{
    xhr = createRequest();
    if (xhr == null)
    {
        alert("Impossible de créer la requête");
        return;
    }
    
    xhr.open('POST',"Voiliers_enregistrer.php",true);
    xhr.onreadystatechange = callback_resultatEnregistrement;
    var cookie = escape(document.cookie);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send("nomVoilier="+document.saisie.nomVoilier.value
            +"&numVoile="+document.saisie.numVoile.value
            +"&classe="+document.saisie.classe.value
            +"&anneeConstr="+document.saisie.anneeConstr.value
            +"&adressePhoto="+document.saisie.adressePhoto.value
            +"&chantier="+document.saisie.chantier.value
            +"&skipper1="+document.saisie.skipper1.value
            +"&skipper2="+document.saisie.skipper2.value
            +"&skippersDescr="+document.saisie.skippersDescr.value
            +"&cookie="+cookie);
}

function callback_resultatEnregistrement()
{
    if (xhr.readyState == 4 && xhr.status == 200)
    {
        if (xhr.responseText=="")
            document.getElementById('msg').innerHTML="enregistrement effectué";
        else
            document.getElementById('msg').innerHTML=xhr.responseText;
    }
}