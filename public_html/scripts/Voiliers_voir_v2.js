window.onload = initPage; //Au chargement de la page faire la méthode initPage
     
function initPage() 
{
   document.getElementById("choix").onchange = afficheSelection;
   xhr = createRequest();   //Création d'un objet xhr de type Requete
   if (xhr == null) //Si echec création
   {
       alert("Echec de la création de la requête"); //Affiche dialogue erreur
       return;
   }
   xhr.onreadystatechange = callback_peupleChoix; //Au changement d'état de la requete, 
                                                  //exécuter la fonction callback_peupleChoix
   xhr.open('GET',"inscrits.xml",true); //Initialisation de la requête (ici changement d'état, exécution de callback_peupleChoix)
   xhr.send(null); //envoi de la requête
}

function callback_peupleChoix()
{
    if (xhr.readyState==4 && xhr.status==200) //test des champs de valeur de la requete
    {
        valeur = xhr.responseXML.getElementsByTagName("nom");   //récupération de tout les éléments "nom" du fichierXML dans un tableau valeur
        div = document.getElementById("choix"); //Récupération dans div des éléments du document père (le fichier HTML) ayant l'id 'choix'
        for (i=0;i<valeur.length;i++) //Parcours du tableau valeur
        {
            elt = document.createElement("option"); //Création d'un elément de type <option>
            noeudTexte = document.createTextNode(valeur[i].textContent); //Création d'un élément de type texte, égal a un nom stocké dans le tableau valeur
            elt.appendChild(noeudTexte); //Ajout de l'élément noeudText à element
            div.appendChild(elt);   //Ajout de l'élement elt à div(soit l'élément avec l'id "choix")
        }
    }
    afficheSelection();
}

function afficheSelection ()	
{
//    alert(document.form.select.value); //Affiche message popup avec la valeur selectionnée
//    document.getElementById("selection").innerHTML = document.form.select.value ; //Ajout de la valeur sélectionnée en tant que texte
    xhr2 = createRequest();
    if (xhr2 == null)
    {
        alert("Echec de la création de la requête");
        return;
    }
    xhr2.onreadystatechange = callback_getVoiliers;
    xhr2.open('GET',"inscrits.xml",true);
    xhr2.send(null);
} //afficheSelection ()

function callback_getVoiliers()
{
    if (xhr2.readyState==4 && xhr2.status==200)
    {
        voilier = xhr2.responseXML.getElementsByTagName("voilier")[document.getElementById("choix").selectedIndex];
        
        //Creation des elements du tableau
        div = document.getElementById("selection");
        table = document.createElement("table");
        ligne1tab = document.createElement("tr");
        celluleImg = document.createElement("td");
        celluleImpData = document.createElement("td");
        ligneDescr = document.createElement("tr");
        celluleDescr = document.createElement("td");
        
        while(div.childNodes.length != 0)
        {
            div.removeChild(div.childNodes[0]);
        }
        
        //Ajout données importantes dans la cellule des données importantes
        celluleImpData.appendChild(document.createTextNode(voilier.getElementsByTagName("nom")[0].textContent));
        celluleImpData.appendChild(document.createElement("br"));
        celluleImpData.appendChild(document.createTextNode("Classe : "+voilier.getAttribute("classe")));
        celluleImpData.appendChild(document.createElement("br"));
        celluleImpData.appendChild(document.createTextNode("Architecte : "+voilier.getElementsByTagName("architecte")[0].textContent));
        celluleImpData.appendChild(document.createElement("br"));
        celluleImpData.appendChild(document.createTextNode("Chantier : "+voilier.getElementsByTagName("chantierNaval")[0].textContent));
        celluleImpData.appendChild(document.createElement("br"));
        celluleImpData.appendChild(document.createTextNode(voilier.getAttribute("anneeFabrication")+", voile numero : "+voilier.getAttribute("numeroDeVoile")));
        
        //Ajout description
        celluleDescr.colSpan=2;
        celluleDescr.appendChild(document.createTextNode("Skippers : "+voilier.getElementsByTagName("skipper")[0].getAttribute("nom")+" "+
                voilier.getElementsByTagName("skipper")[0].getAttribute("prenom")
                +" et "+voilier.getElementsByTagName("skipper")[1].getAttribute("nom")+" "+
                voilier.getElementsByTagName("skipper")[1].getAttribute("prenom")));
        celluleDescr.appendChild(document.createElement("br"));
        celluleDescr.appendChild(document.createTextNode(voilier.getElementsByTagName("texteSkipper")[0].textContent));
        
        //Ajout image
        img = document.createElement("img");
        img.src = voilier.getElementsByTagName("photo")[0].getAttribute("adresse");
        celluleImg.appendChild(img);
        
        //Ajout bordure tableau
        table.border=1;
        
        //Assemblage du tableau
        div.appendChild(table);
        table.appendChild(ligne1tab);
        table.appendChild(ligneDescr);
        ligne1tab.appendChild(celluleImg);
        ligne1tab.appendChild(celluleImpData);
        ligneDescr.appendChild(celluleDescr);
        
    }
}