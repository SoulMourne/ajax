window.onload = initPage; //Au chargement de la page faire la méthode initPage
     
function initPage() 
{
   document.getElementById("choix").onchange = afficheSelection;
   xhr = createRequest();   //Création d'un objet xhr de type Requete
   if (xhr == null) //Si echec création
   {
       alert("Echec de la création de la requête"); //Affiche dialogue erreur
       return;
   }
   xhr.onreadystatechange = callback_peupleChoix; //Au changement d'état de la requete, 
                                                  //exécuter la fonction callback_peupleChoix
   xhr.open('GET',"Voiliers.xml",true); //Initialisation de la requête (ici changement d'état, exécution de callback_peupleChoix)
   xhr.send(null); //envoi de la requête
}

function callback_peupleChoix()
{
    if (xhr.readyState==4 && xhr.status==200) //test des champs de valeur de la requete
    {
        valeur = xhr.responseXML.getElementsByTagName("nom");   //récupération de tout les éléments "nom" du fichierXML dans un tableau valeur
        div = document.getElementById("choix"); //Récupération dans div des éléments du document père (le fichier HTML) ayant l'id 'choix'
        for (i=0;i<valeur.length;i++) //Parcours du tableau valeur
        {
            elt = document.createElement("option"); //Création d'un elément de type <option>
            noeudTexte = document.createTextNode(valeur[i].textContent); //Création d'un élément de type texte, égal a un nom stocké dans le tableau valeur
            elt.appendChild(noeudTexte); //Ajout de l'élément noeudText à element
            div.appendChild(elt);   //Ajout de l'élement elt à div(soit l'élément avec l'id "choix")
        }
    }
}

function afficheSelection ()	
{
    alert(document.form.select.value); //Affiche message popup avec la valeur selectionnée
    document.getElementById("selection").innerHTML = document.form.select.value ; //Ajout de la valeur sélectionnée en tant que texte
} //afficheSelection ()
     
    

        